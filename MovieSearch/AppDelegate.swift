//
//  AppDelegate.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        let _ = FirebaseManager.sharedInstance
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        checkConnectivity()
        
    }
    
    func checkConnectivity() {
        let connected = NetworkReachabilityManager()?.isReachable
        var connectionInfo: [String:Bool] = ["connected":true]
        
        if !connected! {
            connectionInfo["connected"] = false
            let alertView = UIAlertController(title: "Internet Connection", message: "Please make sure that you are connected to the internet", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
                self.checkConnectivity()
            }))
            self.window?.rootViewController?.present(alertView, animated: true, completion: nil)
        }
        
        NotificationCenter.default.post(name: Notification.Name("connectivity"), object: nil, userInfo: connectionInfo)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

