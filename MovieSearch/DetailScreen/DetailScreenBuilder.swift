//
//  DetailScreenBuilder.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class DetailScreenBuilder {
    
    static func make() -> DetailScreenViewController {
        let storyBoard = UIStoryboard(name: "DetailScreen", bundle: nil)
        let view: DetailScreenViewController = storyBoard.instantiateViewController(withIdentifier: "DetailScreenViewController") as! DetailScreenViewController
        
        let interactor = DetailScreenInteractor()
        let router = DetailScreenRouter(viewController: view)
        let presenter = DetailScreenPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
}
