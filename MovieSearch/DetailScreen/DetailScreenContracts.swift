//
//  DetailScreenContracts.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation

protocol DetailScreenInteractorProtocol: class {
    var delegate: DetailScreenInteractorDelegate? { get set }
    func saveMovie(movie: Movie?)
}

protocol DetailScreenInteractorDelegate: class {
    func handleOutput(_ output: DetailScreenInteractorOutput)
}

enum DetailScreenInteractorOutput {
    
}

protocol DetailScreenPresenterProtocol: class {
    func saveMovie(movie: Movie?)
}

enum DetailScreenPresenterOutput {
    
}

protocol DetailScreenViewProtocol: class {
    func handleOutput(_ output: DetailScreenPresenterOutput)
}

protocol DetailScreenRouterProtocol: class {
    func navigate(to route: DetailScreenRoute)
}

enum DetailScreenRoute {
}
