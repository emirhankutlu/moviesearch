//
//  DetailScreenInteractor.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import Firebase

class  DetailScreenInteractor:  DetailScreenInteractorProtocol {
    var delegate: DetailScreenInteractorDelegate?
    
    func saveMovie(movie: Movie?) {
        let movieProperties = movie?.dictionaryRepresentation()
        let reference = Database.database().reference(withPath: "movies")
        reference.childByAutoId().setValue(movieProperties)
    }
}
