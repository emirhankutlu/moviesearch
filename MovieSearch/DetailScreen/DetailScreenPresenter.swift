//
//  DetailScreenPresenter.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class DetailScreenPresenter {
    
    private weak var view: DetailScreenViewProtocol?
    private let interactor: DetailScreenInteractorProtocol
    private let router: DetailScreenRouterProtocol
    private var delegateView: UIViewController!
    
    init(view: DetailScreenViewProtocol, interactor: DetailScreenInteractorProtocol, router: DetailScreenRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.interactor.delegate = self
    }
}

extension DetailScreenPresenter: DetailScreenPresenterProtocol {
    func saveMovie(movie: Movie?) {
        self.interactor.saveMovie(movie: movie)
    }
    
}

extension DetailScreenPresenter: DetailScreenInteractorDelegate {
    func handleOutput(_ output: DetailScreenInteractorOutput) {
        
    }
}
