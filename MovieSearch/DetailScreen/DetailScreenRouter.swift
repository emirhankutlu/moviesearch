//
//  DetailScreenRouter.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class DetailScreenRouter {
    unowned let viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension DetailScreenRouter: DetailScreenRouterProtocol {
    
    func navigate(to route: DetailScreenRoute) {
    }
}
