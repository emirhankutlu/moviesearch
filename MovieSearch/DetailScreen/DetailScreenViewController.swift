//
//  DetailScreenViewController.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Kingfisher

class DetailScreenViewController: UIViewController {
    
    @IBOutlet weak var detailTable: UITableView!
    
    var presenter: DetailScreenPresenterProtocol!
    var movie: Movie?
    var movieProperties: [String:String]?
    var movieKeyList: [String]?
    
    var parallaxHeader: ParallaxHeaderView?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieProperties = movie?.dictionaryRepresentation()
        movieKeyList = movie?.propertyNames()
        
        detailTable.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailTableViewCell")
        detailTable.register(UINib(nibName: "PosterTableViewCell", bundle: nil), forCellReuseIdentifier: "PosterTableViewCell")
        detailTable.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0);
        
        
        let parallaxViewFrame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 200)
        parallaxHeader = ParallaxHeaderView(frame: parallaxViewFrame)
        parallaxHeader?.imageView.kf.setImage(with: URL(string: movie?.poster ?? ""))
        self.detailTable.tableHeaderView  = parallaxHeader
        
        self.presenter.saveMovie(movie: movie)
    }
}

extension DetailScreenViewController: DetailScreenViewProtocol {
    func handleOutput(_ output: DetailScreenPresenterOutput) {
        
    }
}

extension DetailScreenViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (movieKeyList?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var resultCell: UITableViewCell?
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell", for: indexPath) as! DetailTableViewCell
        cell.cellTitle.text = movieKeyList![indexPath.row]
        cell.cellInfo.text = movieProperties![movieKeyList![indexPath.row].capitalize()] as? String
        
        if movieKeyList![indexPath.row] == "imdbRating" {
            cell.cellInfo.text = movie?.imdbRating
        }
        resultCell = cell
        
        return resultCell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        parallaxHeader?.scrollViewDidScroll(scrollView: scrollView)
    }
}
