//
//  StringExtension.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func capitalize() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalize() {
        self = self.capitalize()
    }
}
