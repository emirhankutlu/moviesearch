//
//  UIImageExtension.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func load(urlString: String) {
        DispatchQueue.global().async { [weak self] in
            guard URL(string: urlString) != nil else {
                return
            }
            if let data = try? Data(contentsOf: URL(string: urlString)!) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
