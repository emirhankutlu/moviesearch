//
//  FirebaseManager.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import Firebase

class FirebaseManager {
    
    static let sharedInstance = FirebaseManager()
    
    private init() {
        //loadDefaultValues()
        fetchCloudValues()
    }
    
    func loadDefaultValues() {
        let appDefaults: [String:Any?] = ["launchText":"Loodos"]
        RemoteConfig.remoteConfig().setDefaults(appDefaults as? [String: NSObject])
    }
    
    func fetchCloudValues() {
        
        let fetchDuration: TimeInterval = 0
        RemoteConfig.remoteConfig().fetch(withExpirationDuration: fetchDuration) { status, error in
            
            if let error = error {
                print("Fetching remote values error: \(error)")
                return
            }
            
            RemoteConfig.remoteConfig().activateFetched()
            print("Here: \(RemoteConfig.remoteConfig().configValue(forKey: "launchText").stringValue)")
        }
    }
    
    func string(forkey key: String) -> String{
        return RemoteConfig.remoteConfig()[key].stringValue ?? ""
    }
    
    func activateDebugMode() {
        let debugSettings = RemoteConfigSettings(developerModeEnabled: true)
        RemoteConfig.remoteConfig().configSettings = debugSettings
    }
}
