//
//  LaunchScreenContracts.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation

protocol LaunchScreenInteractorProtocol: class {
    var delegate: LaunchScreenInteractorDelegate? { get set }
    func checkInternetConnectivity()
    func getRemoteValue()
}

protocol LaunchScreenInteractorDelegate: class {
    func handleOutput(_ output: LaunchScreenInteractorOutput)
}

enum LaunchScreenInteractorOutput {
    case notConnected
    case connected
    case getRemoteValue(title: String)
}

protocol LaunchScreenPresenterProtocol: class {
    func checkInternetConnectivity()
    func getRemoteValue()
}

enum LaunchScreenPresenterOutput {
    case notConnected
    case getRemoteValue(title: String)
}

protocol LaunchScreenViewProtocol: class {
    func handleOutput(_ output: LaunchScreenPresenterOutput)
}

protocol LaunchScreenRouterProtocol: class {
    func navigate(to route: LaunchScreenRoute)
}

enum LaunchScreenRoute {
    case mainScreen
}
