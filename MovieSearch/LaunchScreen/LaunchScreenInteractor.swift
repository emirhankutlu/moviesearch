//
//  LaunchScreenInteractor.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import Alamofire
import Firebase

class LaunchScreenInteractor: LaunchScreenInteractorProtocol {
    
    var delegate: LaunchScreenInteractorDelegate?
    
    func getRemoteValue() {
        let launchText = RemoteConfig.remoteConfig().configValue(forKey: "launchText").stringValue
        
        delegate?.handleOutput(.getRemoteValue(title: launchText ?? "Loodosz"))
                
    }
    
    func checkInternetConnectivity() {
        let connection = NetworkReachabilityManager()?.isReachable
        
        if connection! {
            delegate?.handleOutput(.connected)
        }
        else {
            delegate?.handleOutput(.notConnected)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(_:)), name: Notification.Name("connectivity"), object: nil)
    
    }
    
    @objc func notificationReceived(_ notification: Notification) {
        guard let connection = notification.userInfo?["connected"] as? Bool else { return }
        
        if connection {
            delegate?.handleOutput(.connected)
        }
    }
}
