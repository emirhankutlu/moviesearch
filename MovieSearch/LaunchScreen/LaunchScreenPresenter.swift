//
//  LaunchScreenPresenter.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class LaunchScreenPresenter {
    
    private weak var view:LaunchScreenViewProtocol?
    private let interactor: LaunchScreenInteractorProtocol
    private let router: LaunchScreenRouterProtocol
    private var delegateView: UIViewController!
    
    init(view: LaunchScreenViewProtocol, interactor: LaunchScreenInteractorProtocol, router: LaunchScreenRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.interactor.delegate = self
    }
    
}

extension LaunchScreenPresenter: LaunchScreenPresenterProtocol {
    func getRemoteValue() {
        self.interactor.getRemoteValue()
    }
    
    func checkInternetConnectivity() {
        self.interactor.checkInternetConnectivity()
    }
    
}

extension LaunchScreenPresenter: LaunchScreenInteractorDelegate {
    func handleOutput(_ output: LaunchScreenInteractorOutput) {
        switch output {
        case .connected:
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                self.router.navigate(to: .mainScreen)
            })
        case .notConnected:
            view?.handleOutput(.notConnected)
        case .getRemoteValue(let title):
            view?.handleOutput(.getRemoteValue(title: title))
        }
    }
}
