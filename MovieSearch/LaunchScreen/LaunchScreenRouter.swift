//
//  LaunchScreenRouter.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class LaunchScreenRouter {
    unowned let viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension LaunchScreenRouter: LaunchScreenRouterProtocol {
    
    func navigate(to route: LaunchScreenRoute) {
        switch route {
        case .mainScreen:
            let view = MainScreenBuilder.make()
            let navigationController = UINavigationController()
            navigationController.viewControllers = [view]
            UIApplication.shared.windows.first?.rootViewController = navigationController
        }
    }
    
}
