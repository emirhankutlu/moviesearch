//
//  LaunchScreenViewController.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {
    
    @IBOutlet weak var launcLabel: UILabel!
    var presenter: LaunchScreenPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        self.presenter.checkInternetConnectivity()
        self.presenter.getRemoteValue()
    }
    
    func configureView() {
        
        let interactor = LaunchScreenInteractor()
        let router = LaunchScreenRouter(viewController: self)
        let presenter = LaunchScreenPresenter(view: self, interactor: interactor, router: router)
        
        self.presenter = presenter
    }
}

extension LaunchScreenViewController: LaunchScreenViewProtocol {
    func handleOutput(_ output: LaunchScreenPresenterOutput) {
        switch output {
        case .notConnected:
            print("")
        case .getRemoteValue(let title):
            launcLabel.text = title
        }
    }
}
