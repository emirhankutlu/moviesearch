//
//  MainScreenBuilder.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class MainScreenBuilder {
    
    static func make() -> MainScreenViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let view: MainScreenViewController = storyBoard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        
        let interactor = MainScreenInteractor()
        let router = MainScreenRouter(viewController: view)
        let presenter = MainScreenPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        
        return view
    }
}
