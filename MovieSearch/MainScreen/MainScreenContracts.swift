//
//  MainScreenContracts.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation

protocol MainScreenInteractorProtocol: class {
    var delegate: MainScreenInteractorDelegate? { get set }
    func searchMovie(title: String)
    func getMovieDetail(title: String)
}

protocol MainScreenInteractorDelegate: class {
    func handleOutput(_ output: MainScreenInteractorOutput)
}

enum MainScreenInteractorOutput {
    case movieNotFound
    case movieFound(movieList: [MovieSearch]?)
    case getMovieDetails(movie: Movie?)
}

protocol MainScreenPresenterProtocol: class {
    func searchMovie(title: String)
    func getMovieDetail(title: String)
}

enum MainScreenPresenterOutput {
    case movieFound(movieList: [MovieSearch]?)
    case movieNotFound
    case getMovieDetails(movie: Movie?)
}

protocol MainScreenViewProtocol: class {
    func handleOutput(_ output: MainScreenPresenterOutput)
}

protocol MainScreenRouterProtocol: class {
    func navigate(to route: MainScreenRoute)
}

enum MainScreenRoute {
    case movieDetail(movie: Movie?)
}
