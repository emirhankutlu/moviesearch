//
//  MainScreenInteractor.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation

import Foundation
import Alamofire

class  MainScreenInteractor:  MainScreenInteractorProtocol {
    
    var delegate: MainScreenInteractorDelegate?
    
    func getMovieDetail(title: String) {
        Alamofire.request("http://omdbapi.com/?i=tt3896198&apikey=e2489a0e&t=\(title)", method: .get).responseJSON { response in
            
            switch response.result {
            case .success:
                guard let responseDict = response.result.value as? [String:Any] else { return }
                guard let movie = try? Movie(object: responseDict) else { return }
                
                self.delegate?.handleOutput(.getMovieDetails(movie: movie))
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func searchMovie(title: String) {
        Alamofire.request("http://omdbapi.com/?i=tt3896198&apikey=e2489a0e&s=\(title)", method: .get).responseJSON { response in
            
            switch response.result {
            case .success:
                var movieSearchList: [MovieSearch] = []
                guard let responseDict = response.result.value as? [String:Any] else { return }
                
                if let movieFound = responseDict["Response"] as? String{
                    if movieFound == "True"{
                        guard let movieList = responseDict["Search"] as? [[String:Any]] else { return }
                        
                        for movie in movieList {
                            guard let movieSearch = try? MovieSearch(object: movie) else { return }
                            movieSearchList.append(movieSearch)
                        }
                        
                        self.delegate?.handleOutput(.movieFound(movieList: movieSearchList))
                    }
                    else {
                        self.delegate?.handleOutput(.movieNotFound)
                    }
                }
                else {
                    self.delegate?.handleOutput(.movieNotFound)
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
