//
//  MainScreenPresenter.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class MainScreenPresenter {
    
    private weak var view: MainScreenViewProtocol?
    private let interactor: MainScreenInteractorProtocol
    private let router: MainScreenRouterProtocol
    private var delegateView: UIViewController!
    
    init(view: MainScreenViewProtocol, interactor: MainScreenInteractorProtocol, router:MainScreenRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.interactor.delegate = self
    }
}

extension MainScreenPresenter: MainScreenPresenterProtocol {
    func getMovieDetail(title: String) {
        self.interactor.getMovieDetail(title: breakString(title: title))
    }
    
    func searchMovie(title: String) {
        self.interactor.searchMovie(title: breakString(title: title))
    }
    
    func breakString(title: String) -> String {
        let brokenString = title.replacingOccurrences(of: " ", with: "+")
        return brokenString
    }
}

extension MainScreenPresenter: MainScreenInteractorDelegate {
    func handleOutput(_ output: MainScreenInteractorOutput) {
        switch output {
        case .movieFound(let movieList):
            view?.handleOutput(.movieFound(movieList: movieList))
        case .movieNotFound:
            view?.handleOutput(.movieNotFound)
        case .getMovieDetails(let movie):
            router.navigate(to: .movieDetail(movie: movie))
        }
    }
}
