//
//  MainScreenRouter.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import UIKit

class MainScreenRouter {
    unowned let viewController: UIViewController
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

extension MainScreenRouter: MainScreenRouterProtocol {
    
    func navigate(to route: MainScreenRoute) {
        switch route {
        case .movieDetail(let movie):
            let view = DetailScreenBuilder.make()
            view.movie = movie
            viewController.navigationController?.pushViewController(view, animated: true)
        }
    }
    
}
