//
//  MainScreenViewController.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import UIKit
import Kingfisher

class MainScreenViewController: UIViewController {
    
    var presenter: MainScreenPresenterProtocol!
    var movieList: [MovieSearch] = []
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var movieTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    func configureView() {
        movieTableView.register(UINib(nibName: "MovieTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieTableViewCell")
    }
    
    @IBAction func searchMovie(_ sender: Any) {
        presenter.searchMovie(title: searchTextField.text!)
    }
    
}

extension MainScreenViewController: MainScreenViewProtocol {
    func handleOutput(_ output: MainScreenPresenterOutput) {
        switch output {
        case .movieFound(let movie):
            movieList = movie!
            movieTableView.reloadData()
        case .movieNotFound:
            let alert = UIAlertController(title: "No movie found!", message: "Please try another movie.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case .getMovieDetails(let movie):
            print("")
        }
    }
}

extension MainScreenViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(movieList.count)
        return movieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as! MovieTableViewCell
        
        cell.movieTitle.text = movieList[indexPath.row].title
        cell.movieImage.kf.setImage(with: URL(string: movieList[indexPath.row].poster ?? ""))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.getMovieDetail(title: movieList[indexPath.row].title!)
    }
    
    
}
