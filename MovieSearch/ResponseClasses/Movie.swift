//
//  Movie.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import Marshal

class Movie: Unmarshaling {
    
    private struct SerializationKeys {
        static let poster = "Poster"
        static let imdbRating = "imdbRating"
        static let title = "Title"
        static let year = "Year"
        static let released = "Released"
        static let genre = "Genre"
        static let director = "Director"
        static let language = "Language"
        static let country = "Country"
    }
    
    public var imdbRating: String?
    public var title: String?
    public var year: String?
    public var released: String?
    public var genre: String?
    public var director: String?
    public var language: String?
    public var country: String?
    public var poster: String?
    
    required init(object: MarshaledObject) throws {
        poster = try? object.value(for: SerializationKeys.poster)
        imdbRating = try? object.value(for: SerializationKeys.imdbRating)
        title = try? object.value(for: SerializationKeys.title)
        year = try? object.value(for: SerializationKeys.year)
        released = try? object.value(for: SerializationKeys.released)
        genre = try? object.value(for: SerializationKeys.genre)
        director = try? object.value(for: SerializationKeys.director)
        language = try? object.value(for: SerializationKeys.language)
        country = try? object.value(for: SerializationKeys.country)
    }
    
    public func dictionaryRepresentation() -> [String: String] {
        var dictionary: [String: String] = [:]
        if let value = poster { dictionary[SerializationKeys.poster] = value }
        if let value = imdbRating { dictionary[SerializationKeys.imdbRating] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = year { dictionary[SerializationKeys.year] = value }
        if let value = released { dictionary[SerializationKeys.released] = value }
        if let value = genre { dictionary[SerializationKeys.genre] = value }
        if let value = director { dictionary[SerializationKeys.director] = value }
        if let value = language { dictionary[SerializationKeys.language] = value }
        if let value = country { dictionary[SerializationKeys.country] = value }
        return dictionary
    }
}

extension Movie {
    func propertyNames() -> [String] {
        return Mirror(reflecting: self).children.flatMap { $0.label }
    }
}
