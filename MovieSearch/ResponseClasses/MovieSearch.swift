//
//  MovieSearch.swift
//  MovieSearch
//
//  Created by Emirhan Kutlu on 6.03.2020.
//  Copyright © 2020 Emirhan Kutlu. All rights reserved.
//

import Foundation
import Marshal

class MovieSearch: Unmarshaling {
    
    private struct SerializationKeys {
        static let title = "Title"
        static let poster = "Poster"
    }
    
    public var title: String?
    public var poster: String?
    
    required init(object: MarshaledObject) throws {
        title = try? object.value(for: SerializationKeys.title)
        poster = try? object.value(for: SerializationKeys.poster)
    }
    
}
